package TestPackage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Test_Case_2 extends RequestBody {

	public static void executor() throws ClassNotFoundException, IOException {

		File dir_name = Utility.CreateLogDirectory("Post_API_Logs");

		String Endpoint = RequestBody.Hostname() + RequestBody.Patch_Update();
		Response response = API_Trigger.Trigger_Patch(RequestBody.HeaderName(), RequestBody.HeaderValue(),
				RequestBody.Body_Patch_Update(), Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("Test_Case_2"), dir_name, Endpoint, RequestBody.Body_Patch_Update(),
				response.getHeader("Date"), response.getBody().asString());

		// Extract the response parameters
		int statuscode = response.statusCode();
		ResponseBody res_body = response.getBody();
		String res_name = res_body.jsonPath().getString("name");
		String res_job = res_body.jsonPath().getString("job");
		String res_updatedAt = res_body.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);

		// Set the expected results
		JsonPath jsp_req = new JsonPath(RequestBody.Body_Patch_Update());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// Validate the response parameters
		Assert.assertEquals(statuscode, 200);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		//Assert.assertEquals(res_updatedAt, expecteddate);

	}

}