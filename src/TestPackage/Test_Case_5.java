package TestPackage;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.RequestBody;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Test_Case_5 extends RequestBody {

	public static void executor() throws ClassNotFoundException, IOException {

		File dir_name = Utility.CreateLogDirectory("Post_API_Logs");

		String Endpoint = RequestBody.Hostname() + RequestBody.Patch_Update();
		Response response = API_Trigger.Trigger_Get(RequestBody.HeaderName(), RequestBody.HeaderValue(), Endpoint);

		//Utility.evidenceFileCreator(Utility.testLogName("Test_Case_2"), dir_name, Endpoint, RequestBody.Body_Patch_Update(),
				//response.getHeader("Date"), response.getBody().asString());

		// Extract the response parameters
		int statuscode = response.statusCode();
		
		Assert.assertEquals(statuscode, 200);
	
	}

}