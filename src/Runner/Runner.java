package Runner;

import java.io.IOException;

import TestPackage.Test_Case_1;
import TestPackage.Test_Case_2;
import TestPackage.Test_Case_3;
import TestPackage.Test_Case_4;
import TestPackage.Test_Case_5;
import TestPackage.Test_Case_6;

public class Runner {

	public static void main(String[] args) throws ClassNotFoundException, IOException {
		Test_Case_1.executor();
		Test_Case_2.executor();
		Test_Case_3.executor();
		Test_Case_4.executor();
		Test_Case_5.executor();
		Test_Case_6.executor();
	}

}