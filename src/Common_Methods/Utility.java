
package Common_Methods;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalTime;

import io.restassured.response.ResponseBody;

public class Utility {

	public static void evidenceFileCreator(String Filename, File FileLocation, String endpoint, String RequestBody,
			String ResHeader, String ResponseBody) throws IOException {
		// Step 1 : Create and Open the file
		File newTextFile = new File(FileLocation + "\\" + Filename + ".txt");
		System.out.println("File create with name: " + newTextFile.getName());

		// Step 2 : Write data
		FileWriter writedata = new FileWriter(newTextFile);
		writedata.write("Endpoint is :\n" + endpoint + "\n\n");
		writedata.write("Request body is :\n" + RequestBody + "\n\n");
		writedata.write("Response header date is : \n" + ResHeader + "\n\n");
		writedata.write("Response body is : \n" + ResponseBody);

		// Step 3 : Save and close
		writedata.close();

	}

	public static File CreateLogDirectory(String dirName) {

		// Step 1 : Fetch the Java project name and location

		String projectDir = System.getProperty("user.dir");
		System.out.println("Current project Directory Is :" + projectDir);

		// Step 2 : Verify weather the directory in variable dirName exists inside the
		// projectDir and act accordingly

		File directory = new File(projectDir + "\\" + dirName);

		if (directory.exists()) {
			System.out.println(directory + " , already exists");
		} else {
			System.out.println(directory + " , doesnt exists , hence creating it");
			directory.mkdir();
			System.out.println(directory + " , created");
		}

		return directory;
	}
	
	public static String testLogName (String Name) {
		LocalTime currentTime = LocalTime.now();
		String currentstringTime=currentTime.toString().replaceAll(":", "");
		String TestLogName = "Test_Case_1"+currentstringTime;
		return TestLogName;
	}

}