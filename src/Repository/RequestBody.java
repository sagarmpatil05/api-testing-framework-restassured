package Repository;

import java.io.IOException;
import java.util.ArrayList;

import Common_Methods.Utility_DataRunner;

public class RequestBody extends Environment {
	
	
	public static String req_post_tc(String TestCaseName) throws IOException {
		ArrayList<String> Data = Utility_DataRunner.readExcelData("Post_API", TestCaseName);
		String key_name = Data.get(1);
		String value_name = Data.get(2);
		String key_job = Data.get(3);
		String value_job = Data.get(4);
		String req_body = "{\r\n" + "    \"" + key_name + "\": \"" + value_name + "\",\r\n" + "    \"" + key_job
				+ "\": \"" + value_job + "\"\r\n" + "}";

		return req_body;
	}

	public static String req_tc1() {
		
		String req_body ="{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}";

		return req_body;
	}
	
	
	public static String Body_Post_Register_Successful() {

		String body__post_register_successful = "{\r\n" + "    \"email\": \"eve.holt@reqres.in\",\r\n"
				+ "    \"password\": \"pistol\"\r\n" + "}";

		return body__post_register_successful;
	}
	
	

	public static String Body_Post_Register_UnSuccessful() {

		String body__post_register_Unsuccessful = "{\r\n" + "    \"email\": \"sydney@fife\"\r\n" + "}";

		return body__post_register_Unsuccessful;
	}
	
	

	public static String Body_Post_Login_Successful() {

		String body__post_login_successful = "{\r\n" + "    \"email\": \"eve.holt@reqres.in\",\r\n"
				+ "    \"password\": \"cityslicka\"\r\n" + "}";

		return body__post_login_successful;
	}
	
	

	public static String Body_Post_Login_UnSuccessful() {

		String body__post_login_unsuccessful = "{\r\n" + "    \"email\": \"peter@klaven\"\r\n" + "}";

		return body__post_login_unsuccessful;
	}
	
	

	public static String Body_Patch_Update() {

		String body_patch_update = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n"
				+ "}";

		return body_patch_update;
	}
	
	
	
	public static String Body_Put_Update() {

		String body_put_update = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";

		return body_put_update;
	}

}
