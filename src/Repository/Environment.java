package Repository;

public class Environment {

	public static String Hostname() {
		String hostname = "https://reqres.in/";
		return hostname;
	}

	public static String Resource() {
		String resource = "api/users";
		return resource;
	}

	public static String HeaderName() {

		String headername = "Content-Type";
		return headername;
	}

	public static String HeaderValue() {

		String headervalue = "application/json";
		return headervalue;
	}
	
	
	public static String Patch_Update() {
		String patch_update = "api/users/2";
		return patch_update;
	}

	
	public static String Source_Put_Update() {
		String source_put_update = "api/users/2";
		return source_put_update;
	}
	
	public static String Source_Delete() {
		String source_put_update = "api/users/2";
		return source_put_update;
	}



}