<h1>Api Testing Framework RestAssured</h1>

<h3>Discription</h3>

This Framework designed for efficient and expressive testing of RESTful APIs. With its clean syntax, support for various HTTP methods, and robust validation capabilities, it simplifies the creation of test scripts,data driven testing using excel and json file, evidence creator which make it more usefull in testing purpose.

<h3>Framework used</h3>

- RestAssured
- TestNG
- Apache POI

<h3>Features</h3>


1. Validate the Response<br>
2. Able to do the data driven testing <br>
3. Have runner to execute multiple files in one click
4. Able to generate log directory


